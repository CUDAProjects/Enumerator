#include <stdio.h>
#include <cuda_runtime_api.h>

int main(int argc, char *argv[])
{
	int NumberOfDevices;
	struct cudaDeviceProp DeviceProperties;
	int i;

	cudaGetDeviceCount(&NumberOfDevices);

	if(NumberOfDevices > 0)
	{
		for(i = 0; i < NumberOfDevices; i++)
		{
			printf("\033[1;34m<<<\033[1;33mDevice %i\033[1;34m>>>\033[0m\n\n", i);

			cudaGetDeviceProperties(&DeviceProperties, i);

			printf("\033[1;31mDevice name:\033[1;37m %s\n", DeviceProperties.name);
			printf("\033[1;31mClock frequency (Mhz):\033[1;37m %i\n", DeviceProperties.clockRate / 1000);
			printf("\033[1;31mCompute capability:\033[1;37m %i.%i\n", DeviceProperties.major, DeviceProperties.minor);
			printf("\033[1;31mNumber of processors:\033[1;37m %i\n", DeviceProperties.multiProcessorCount * 8);
			printf("\033[1;31mMax Grid size (X,Y,Z):\033[1;37m (%i,%i,%i)\n", DeviceProperties.maxGridSize[0], DeviceProperties.maxGridSize[1], DeviceProperties.maxGridSize[2]);
			printf("\033[1;31mMax Block size (X,Y,Z):\033[1;37m (%i,%i,%i)\n", DeviceProperties.maxThreadsDim[0], DeviceProperties.maxThreadsDim[1], DeviceProperties.maxThreadsDim[2]);
			printf("\033[1;31mMax Threads per block:\033[1;37m %i\n", DeviceProperties.maxThreadsPerBlock);
			printf("\033[1;31mGlobal Memory size (Mb):\033[1;37m %li\n", (DeviceProperties.totalGlobalMem / 1024) / 1024);
			printf("\033[1;31mConstant Memory size (Kb):\033[1;37m %li\n", DeviceProperties.totalConstMem / 1024);
			printf("\033[1;31mShared Memory available per block (Kb):\033[1;37m %li\n", DeviceProperties.sharedMemPerBlock / 1024);
			printf("\033[1;31mRegisters per block:\033[1;37m %i\n", DeviceProperties.regsPerBlock);
			printf("\033[1;31mWarp size (threads):\033[1;37m %i\n", DeviceProperties.warpSize);
			printf("\033[1;31mTexture alignment:\033[1;37m %li\n", DeviceProperties.textureAlignment);
			printf("\033[1;31mMaximum pitch allowed by memory copies (Kb):\033[1;37m %li\n", DeviceProperties.memPitch / 1024);

			printf("\033[1;31mCan map Host Memory?:\033[1;37m ");

			if(DeviceProperties.canMapHostMemory == 1)
			{
				printf("Yes\n");
			}
			else
			{
				printf("No\n");
			}

			printf("\033[1;31mCompute mode:\033[1;37m ");

			switch(DeviceProperties.computeMode)
			{
				case cudaComputeModeDefault:
					printf("Default\n");
					break;
				case cudaComputeModeExclusive:
					printf("Exclusive\n");
					break;
				case cudaComputeModeProhibited:
					printf("Prohibited\n");
					break;
				default:
					printf("?\n");
					break;
			}

			printf("\033[1;31mCan execute a kernel and copy memory?:\033[1;37m ");

			if(DeviceProperties.deviceOverlap == 1)
			{
				printf("Yes\n");
			}
			else
			{
				printf("No\n");
			}

			printf("\033[1;31mIntegrated?:\033[1;37m ");

			if(DeviceProperties.integrated == 1)
			{
				printf("Yes\n");
			}
			else
			{
				printf("No\n");
			}

			printf("\033[1;31mRun time limit on kernels?:\033[1;37m ");

			if(DeviceProperties.kernelExecTimeoutEnabled == 1)
			{
				printf("Yes\n\n");
			}
			else
			{
				printf("No\n\n");
			}
		}
	}
	else
	{
		printf("Sorry. There are no CUDA devices on your system :(\n\n");
	}

	return 0;
}
